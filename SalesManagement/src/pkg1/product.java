package pkg1;

public class product {
	private int id;
	private String name;
	private double price;
	private int quantity;
	private double amount;

	public product() {
		this.id = 0;
		this.name = "";
		this.price = 0.0;
		this.quantity = 0;
		this.amount = 0.0;

	}

	public product(int id, String name, double price, int quantity, double amount) {

		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.amount = amount;

	}

	public product(product p) {
		this.id = p.id;
		this.name = p.name;
		this.price = p.price;
		this.quantity = p.quantity;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "product [id=" + id + ", name=" + name + ", price=" + price + ", quantity=" + quantity + ", amount="
				+ amount + "]";
	}

}
