package pkg1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SaveData {
	private Connection conn;
	private void db_connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/product_db","root","");
			
			
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
	private void db_close() {
		try {
			conn.close();
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void saveData(product p) {
		
		String sql="insert into tbl_product values(?,?,?,?,?)";
		try {
			db_connect();
			PreparedStatement pstat=conn.prepareStatement(sql);
			
			pstat.setInt(1,p.getId());
			pstat.setString(2,p.getName());
			pstat.setDouble(3,p.getPrice());
			pstat.setInt(4,p.getQuantity());
			pstat.setDouble(5,p.getAmount());
			pstat.executeUpdate();
			
			pstat.close();
			db_close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

}
