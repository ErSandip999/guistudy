package com.infotech.people.managementapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotech.people.managementapp.dao.PeopleManagementDao;
import com.infotech.people.managementapp.entities.Person;

@Service
public class PeopleManagementService {
	@Autowired
	private PeopleManagementDao peopleManagementDao;

	public Person createPerson(Person p1) {
		return peopleManagementDao.save(p1);
		
		
	}

	public void createPersons(List<Person> personList) {
		peopleManagementDao.saveAll(personList);
		
	}

	public Iterable<Person> getPersonsbyIds(List<Integer> ids) {
		return peopleManagementDao.findAllById(ids);
		 
	}

	public List<Person> getPersonsInfoByLastName(String lastName) {
		
		return peopleManagementDao.findIdByLastName(lastName);
	}

	public List<Person> getPersonsInfoByFirstNameAndEmail(String firstName, String lastName) {
		
		return peopleManagementDao.findIsByFirstNameAndEmail(firstName, lastName);
	}

}
