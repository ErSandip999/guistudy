package com.infotech.people.managementapp.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.infotech.people.managementapp.entities.Person;

public interface PeopleManagementDao extends CrudRepository<Person,Integer>{
	List<Person> findIdByLastName(String lastName);
	List<Person>findIsByFirstNameAndEmail(String firstName,String lastName);

}
