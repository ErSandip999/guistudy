package Client;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.Employee;

public class Test {
	public static void main(String[] args) {
		AbstractApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		Employee e=context.getBean("employee",Employee.class);
		System.out.println(e);
		context.close();
				
	}

}
