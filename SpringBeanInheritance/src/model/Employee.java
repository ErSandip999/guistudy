package model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Employee implements InitializingBean,DisposableBean {
	private int id;
	private String name;
	private String gender;
	private String PAN;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPAN() {
		return PAN;
	}
	public void setPAN(String pAN) {
		PAN = pAN;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", gender=" + gender + ", PAN=" + PAN + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getGender()=" + getGender() + ", getPAN()=" + getPAN()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	@Override
	public void destroy() throws Exception {
		System.out.println("Bean is going through Destroy Process....");
		
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Bean is going through Init process...");
	
		
	}
	
	

}
