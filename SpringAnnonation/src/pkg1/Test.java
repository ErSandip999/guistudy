package pkg1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
		Samsung s7=context.getBean(Samsung.class);
		s7.print();

	}

}
