<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JS-Variables</title>
</head>
<body>
<h2>JavaScript Statements</h2>

<p>A <b>JavaScript program</b> is a list of <b>statements</b> to be executed by a computer.</p>

<p id="demo"></p>

<button type="button" onclick="myFunction5()">Add</button>
<p>(myFunction is stored in an external file called "test.js")</p>
	<script type="text/javascript" src="<c:url value='/resources/js/test.js' />"/>
</body>
</html>