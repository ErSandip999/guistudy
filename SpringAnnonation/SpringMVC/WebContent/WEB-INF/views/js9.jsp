<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<title>BootStrap</title>
</head>
<body>
<div class="jumbotron text-center">
  <h1>My First Bootstrap Page</h1>
  <p>Resize this responsive page to see the effect!</p> 
</div>
  
<div class="container">
  <div class="row">
    <div class="col-sm-4">
      <h3>Column 1</h3>
      <p>I am practicing to integrete bootstrap...</p>
      <p>So far its going great...</p>
    </div>
    <div class="col-sm-4">
      <h3>Column 2</h3>
      <p>i dont know about all the classes yet</p>
      <p>But i will soon learn it ...</p>
    </div>
    <div class="col-sm-4">
      <h3>Column 3</h3>        
      <p>Hope it is not too difficult to learn...</p>
      <p>lets hope to caomplete the tutorial...</p>
    </div>
  </div>
</div>


</body>
</html>