<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">

<title>JS-Function</title>
</head>
<body>
<p id="demo">A Paragraph.</p>

<button type="button" onclick="myFunction3()">Try it</button>

<p>(myFunction is stored in an external file called "test.js")</p>
	<script type="text/javascript" src="<c:url value='/resources/js/test.js' />"></script>
</body>

</html>