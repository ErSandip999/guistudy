<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Link Page</title>
</head>
<body>
<p><a href="intro">JavaScript-Intro</a></p>
<p><a href="js2">JavaScript-Page2</a></p>
<p><a href="js3">JavaScript-Page3</a></p>
<p><a href="js4">JavaScript-Page4</a></p>
<p><a href="js5">JavaScript-Page5</a></p>
<p><a href="js6.html">JavaScript-Page6</a></p>
<p><a href="js7">JavaScript-Page7</a></p>
<p><a href="js8">JavaScript-Page8</a></p>
<p><a href="form1">JavaScript-Form</a></p>
<p><a href="js9">BootStrap-Page1</a></p>
<p><a href="bootstrap10">BootStrap-Page2</a></p>
<p><a href="bootstrap2">BootStrap-Page3</a></p>
<p><a href="css">scc.jsp</a></p>


</body>
</html>