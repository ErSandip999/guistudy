function myFunction() {
	document.getElementById("demo").innerHTML = "Paragraph changed.";
}
function myFunction2() {
	window.alert("Hello world of JavaScript");
}
function myFunction3() {
	document.write("the Sum of 5 and 6 is 11")
}
function myFunction4() {
	console.log(5 + 6);
}
function myFunction5() {
	var x, y, z;
	x = 400;
	y = 500;
	z = x + y;
	document.getElementById("demo").innerHTML = "the value of z is:" + z;
}
function myFunction6() {
	var cars = [ "saab", "volvo", "ford" ];// javascript arrays
	var person = {
		firstName : "John",
		lastName : "Doe",
		age : 50,
		eyeColor : "blue"
	};// here person is a javascript object....

	document.getElementById("demo").innerHTML = person.firstName + ""
			+ person.lastname + " is " + person.age + " years old.";

}
function myFunction7() {
	var person = {
		firstName : "John",
		lastName : "Doe",
		id : 5566,
		fullName : function() {
			return this.firstName + " " + this.lastName;
		}
	};
}
function validateForm() {
	 var x = document.forms["myForm"]["first_name"].value;
	//var x = document.getElementbyId("id1");
	var y = document.forms["myForm"]["family_name"].value;
	var z = document.forms["myForm"]["age"].value;
	if (x == "" || y == "" || z == "") {
		window.alert("Every Field must be filled out");
		return false;
	
	}
	if(z<20){
		window.alert("You are too young to fill this form...");
		return false;
	}	

}