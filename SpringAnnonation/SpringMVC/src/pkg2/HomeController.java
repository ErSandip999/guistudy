package pkg2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping("/intro")
	public String intro() {
		return "jsIntro";
	}
	
	@RequestMapping("/js2")
	public String getjs2() {
		return "js2";
	}
	
	@RequestMapping("/js3")
	public String getjs3() {
		return "js3";
	}
	
	@RequestMapping("/js4")
	public String getjs4() {
		return "js4";
	}
	
	@RequestMapping("/js5")
	public String getjs5() {
		return "js5";
	}
	
	@RequestMapping("/js7")
	public String getjs7() {
		return "js7";
		
	}
	
	@RequestMapping("/js8")
	public String getjs8() {
		return "js8";
	}
	
	@RequestMapping("/js9")
	public String getjs9() {
		return "js9";
	}
	
	@RequestMapping("/bootstrap10")
	public String getbootstrap10() {
		return "bootstrap10";
	}
	
	@RequestMapping("/form1")
	public String getForm() {
		return "form1";
	}
	
	@RequestMapping("/bootstrap2")
	public String getBootstrap2() {
		return "bootstrap2";
	}
	@RequestMapping("/css")
	public String getscc() {
		return "scc";
		
	}

}
