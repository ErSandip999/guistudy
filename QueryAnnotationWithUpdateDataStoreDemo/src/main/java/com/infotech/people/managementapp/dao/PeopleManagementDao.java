package com.infotech.people.managementapp.dao;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import com.infotech.people.managementapp.entities.Person;

public interface PeopleManagementDao extends CrudRepository<Person,Integer>{
	List<Person> findIdByLastName(String lastName);
	List<Person>findIsByFirstNameAndEmail(String firstName,String lastName);
	
	
	@Transactional
	@Modifying
	@Query(value="Update Person set Email=:newEmail where id=:personId")
	void updatePersonEmailById(@Param("personId")int id,@Param("newEmail") String newEmail);
}
