package com.infotech.people.managementapp.service;


import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.infotech.people.managementapp.dao.PeopleManagementDao;
import com.infotech.people.managementapp.entities.Person;

@Service
public class PeopleManagementService {
	@Autowired
	private PeopleManagementDao peopleManagementDao;
	
	
	public void updatePersonEmailById(int id, String newEmail) {
		peopleManagementDao.updatePersonEmailById(id,newEmail);
		
	}

	

	


}
