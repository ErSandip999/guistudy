package pkg1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		Student student=context.getBean("stu1",PrimaryStudent.class);
		System.out.println(student);
		student=context.getBean("stu3",PrimaryStudent.class);
		System.out.println(student);
		student=context.getBean("stu4",PrimaryStudent.class);
		System.out.println(student);
		student=context.getBean("stu5",PrimaryStudent.class);
		System.out.println(student);
		student=context.getBean("stu6",PrimaryStudent.class);
		System.out.println(student);
		student=context.getBean("stu7",PrimaryStudent.class);
		System.out.println(student);
		Student s2=context.getBean("stu7",PrimaryStudent.class);
		s2=context.getBean("stu7",PrimaryStudent.class);
		System.out.println(s2);
		Student s3=context.getBean("stu7",PrimaryStudent.class);
		s3=context.getBean("stu7",PrimaryStudent.class);
		System.out.println(s3);
		context.close();

	}

}
