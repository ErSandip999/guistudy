package Client;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import Service.Communication;

public class Test {

	public static void main(String[] args) {
		AbstractApplicationContext context= new ClassPathXmlApplicationContext("AppContext.xml");
		Communication comm=context.getBean("communication",Communication.class);
		comm.communicate();
		context.close();

	}

}
