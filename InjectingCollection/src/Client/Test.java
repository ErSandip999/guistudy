package Client;

import java.util.List;
import java.util.Set;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import Information.OrgInfo;
import Information.Student;

public class Test {
	public static void main(String[] args) {
		AbstractApplicationContext context =new ClassPathXmlApplicationContext("AppContext.xml");
		OrgInfo o=context.getBean("orgInfo",OrgInfo.class);
		
		Student[]students=o.getStudents();
		for(Student temp:students) {
			System.out.println(temp);
		}
		
		
		String[]names=o.getNames();
		for(String temp:names) {
			System.out.println(temp);
		}
		
		List<String> namelist=o.getEmpNameList();
		for(String list:namelist) {
			System.out.println(list);
		}
		
		Set<Integer>ids=o.getEmpIdSet();
		for(Integer id:ids) {
			System.out.println(id);
		}
		context.close();
	}

}
