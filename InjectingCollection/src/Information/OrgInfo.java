package Information;

import java.util.List;
import java.util.Set;

public class OrgInfo {
	
	private Student[]students=new Student[3];
	private List<Student>studentlist;
	private String[] names=new String[3];
	private List<String> empNameList;
	private Set<Integer> empIdSet;
	
	
	public String[] getNames() {
		return names;
	}
	public void setNames(String[] names) {
		this.names = names;
	}
	public List<String> getEmpNameList() {
		return empNameList;
	}
	public void setEmpNameList(List<String> empNameList) {
		this.empNameList = empNameList;
	}
	public Set<Integer> getEmpIdSet() {
		return empIdSet;
	}
	public void setEmpIdSet(Set<Integer> empIdSet) {
		this.empIdSet = empIdSet;
	}
	public Student[] getStudents() {
		return students;
	}
	public void setStudents(Student[] students) {
		this.students = students;
	}
	public List<Student> getStudentlist() {
		return studentlist;
	}
	public void setStudentlist(List<Student> studentlist) {
		this.studentlist = studentlist;
	}
	
	
	

}
