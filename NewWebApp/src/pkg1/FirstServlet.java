package pkg1;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		
		int id=Integer.parseInt(request.getParameter("id"));
		out.println("The Entered id is "+id);
		String name=(request.getParameter("name")).toUpperCase();
		out.println(name);
		String email=request.getParameter("email");
		out.println(email);
		/*if(id!=0&&name!=null) {
			database db=new database();
			db.insertRecord(id, name);	
		}*/
		String [] languages=request.getParameterValues("language");
		if(languages!=null) {
			out.println("The Languages are:");
			for(String lang:languages) {
				out.println(lang);
			}
			
			
		}
		String gender=request.getParameter("gender");
		out.println("Gender is "+gender);
		String jobcat=request.getParameter("jobCat");
		out.println(jobcat);
		database db=new database();
		db.insertRecord(id, name);	
		
	}

	
}
