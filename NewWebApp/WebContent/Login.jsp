<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Web Application</title>
</head>
<body>
<form method="post"  action="first">
<h1>Login Successful</h1>
<h2>Time of Login is:<br><%=new java.util.Date() %></h2><br>
<h3>Request User Agent:<br><%=request.getHeader("User-Agent") %></h3><br>
<h3>Request language:<%=request.getLocale() %></h3>

<b>Enter Your ID:</b><br>
<input type="text" name="id"><br>
<b>Enter Your Name:</b><br>
<input type="text" name="name"><br>

Enter Your Email:<br>
<input type="text" name="email"><br>
Speaking Language:<br>
<input type="checkbox" name="language" value="english">English<br>
<input type="checkbox" name="language" value="french" >French
<br>
Gender:<br>
<input type="radio" name="gender" value="male">Male<br>
<input type="radio" name="gender" value="female">Female<br>
Job Category:
<select name="jobCat">
    <option value="tech">Technology</option>
    <option value="admin">Administration</option>
    <option value="biology">Biology</option>
    <option value="science">Science</option>
</select><br><br><br>
<input type="submit" value="Submit">

</form>

</body>
</html>