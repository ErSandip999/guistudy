package com.infotech.book.ticket.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotech.book.ticket.app.dao.TicketBookingDao;
import com.infotech.book.ticket.app.entities.Ticket;

@Service
public class TicketBookingService {
	@Autowired
	private TicketBookingDao ticketBookingDao;

	public Ticket createTicket(Ticket ticket) {
		
		return ticketBookingDao.save(ticket);
	}

	

	public Iterable<Ticket> getAllBookedTickets() {
		
		return ticketBookingDao.findAll();
	}

	public void deleteTicket(Integer ticketId) {
		ticketBookingDao.deleteById(ticketId);
		
	}



	public Optional<Ticket> getTicketById(int ticketId) {
		
		return ticketBookingDao.findById(ticketId);
	}
	
	public Ticket updateTicket(Ticket updatedticket) {
		Optional<Ticket> ticketfound=ticketBookingDao.findById(updatedticket.getTicketId());
		Ticket ticketFound= ticketfound.get();
		ticketFound.setBookingDate(updatedticket.getBookingDate());
		ticketFound.setDestStation(updatedticket.getDestStation());
		
		return ticketFound;
		
	}
	 
}
