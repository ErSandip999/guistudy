package Collections;

public class InterestCalculation {

	public static void main(String[] args) {
		final double startrate=10;
		final int nyears=10;
		final int nrates=6;
		//set interest rate from 10 to 15%
		double[]interestrate=new double[nrates];
		for(int j=0;j<interestrate.length;j++) {
			interestrate[j]=(startrate+j)/100;
			
		}
		double[][]balance=new double[nyears][nrates];
		for(int j=0;j<balance[0].length;j++) {
			balance[0][j]=10000;
		}
		for(int i=0;i<balance.length;i++) {
			for (int j=0;j<balance[i].length;j++) {
				double oldbalance=balance[i-1][j];
				double interest=oldbalance*interestrate[j];
				balance[i][j]=oldbalance+interest;
				
			}
		}
		for (int j = 0; j < interestrate.length; j++)
			 System.out.printf("%9.0f%%", 100 * interestrate[j]);
			
			 System.out.println();
			 // print balance table
			 for (double[] row : balance)
			{
			 // print table row
			 for (double b : row)
			System.out.printf("%10.2f", b);
			
			 System.out.println();
			 }
		
		

	}

}
