package Generics;



class Container<T>{
	T value;
	
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public void show() {
		System.out.println(value.getClass().getName());
	}
	
}

public class GenericDemo {
	public static void main(String[]args) {
		/*int value=5;
		List values=new ArrayList();
		values.add(5);
		values.add(7);
		values.add("Sandip");
		System.out.println(values.get(0));
		String name=values.get(2).toString();
		//we can also use type cast method
		System.out.println(name);*/
		Container<Double> obj=new Container<>();
		obj.value=9.9;
		obj.show();
		
	}

}
