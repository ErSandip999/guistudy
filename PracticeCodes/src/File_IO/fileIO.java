package File_IO;
import java.io.*;

public class fileIO {




	public void fw_write(String file_name, String str) {
		File file;
		FileWriter fw;
		try {
			file=new File(file_name);
			fw=new FileWriter(file,true);
			fw.write(str);
			fw.close();
			file=null;
			
			
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
	}
	public String fr_read(String file_name) {
		File file;
		FileReader fr;
	    String temp="";
		int ch;
		try {
			file=new File(file_name);
			fr=new FileReader(file);
			
			
			while((ch=fr.read())!=-1) {
				temp=temp+(char)ch;
			}
			fr.close();
			
			
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
			
		}
		return temp;
	}
	public void br_write(String file_name,String str) {
		File file;
		FileWriter fw;
		BufferedWriter bw;
		try {
			file=new File(file_name);
			fw=new FileWriter(file,true);
			bw=new BufferedWriter(fw);
			bw.write(str);
			bw.close();
			fw.close();
			file=null;
			
			
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
		}
	}
	public String br_read(String file_name) {
		File file;
		FileReader fr;
		BufferedReader br;
		String temp="";
		
		try {
			file=new File(file_name);
			fr=new FileReader(file);
			br=new BufferedReader(fr);
			String line;
			while((line=br.readLine())!=null) {
				temp=temp+line;
				line=br.readLine();
				
				
			}
			
		}catch(Exception ex) {
			System.out.println("error:"+ex.getMessage());
		}
		return temp;
	}
	

}
