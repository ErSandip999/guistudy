package Client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.Message;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		Message m1=context.getBean("message",Message.class);
		System.out.println(m1);
		((AbstractApplicationContext)context).close();
	}

}
