package com.infotech.book.ticket.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TicketBookingManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketBookingManagementApplication.class, args);
	}
	
	/*
	  @Bean public CacheManager cacheManager(){ 
	  return new ConcurrentMapCacheManager("ticketsCache"); //paratemer takes list as arguments so we can write more cache names
	  
	  } */
}
