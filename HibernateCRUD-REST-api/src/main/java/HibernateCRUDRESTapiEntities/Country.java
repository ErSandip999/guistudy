package HibernateCRUDRESTapiEntities;

public class Country {
	private int countryid;
	private String countryname;
	private long population;
	
	public Country() {
		
	}

	public Country(int countryid, String countryname, long population) {
		super();
		this.countryid = countryid;
		this.countryname = countryname;
		this.population = population;
	}

	public int getCountryid() {
		return countryid;
	}

	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}

	public String getCountryname() {
		return countryname;
	}

	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}

	public long getPopulation() {
		return population;
	}

	public void setPopulation(long population) {
		this.population = population;
	}
	

}
