package HibernateCRUDRESTapiServices;

import java.util.HashMap;

import HibernateCRUDRESTapiEntities.Country;



public class CountryServices {
	static HashMap<Integer,Country> countryIdMap=getCountryIdMap();
	public static HashMap<Integer, Country> getCountryIdMap() {
		return countryIdMap;
	}
	public CountryServices() {
		
		if(countryIdMap==null) {
			countryIdMap=new HashMap<Integer, Country>();
			Country india=new Country(12345,"India",1100000);
			Country nepal=new Country(12346,"Nepal",243566);
			
			countryIdMap.put(1, india);
			countryIdMap.put(2, nepal);
		}
		
	}
	

}
