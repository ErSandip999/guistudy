package HibernateCRUDRESTapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateCrudRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateCrudRestApiApplication.class, args);
	}

}
