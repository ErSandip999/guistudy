package com.sandip.Controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController  {
	@GetMapping("/welcome")
	public String sayWelcome() {
		return "Spring Boot App Says Hello...";
	}

	

}
