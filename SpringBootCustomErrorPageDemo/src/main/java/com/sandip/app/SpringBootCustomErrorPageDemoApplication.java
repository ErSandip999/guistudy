package com.sandip.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.sandip.Controller")
public class SpringBootCustomErrorPageDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCustomErrorPageDemoApplication.class, args);
	}

}
