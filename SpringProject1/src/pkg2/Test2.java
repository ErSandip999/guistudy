package pkg2;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2 {

	public static void main(String[] args) {
		//load spring configuration file
		ClassPathXmlApplicationContext context= new ClassPathXmlApplicationContext("AppContext.xml");
		//Retrieve bean from spring container
		PersonInf obj1 =context.getBean("myStudent",Student.class);
		//Call method on the bean
		System.out.println(obj1.get_message());
		obj1=context.getBean("myPerson",Person.class);
		System.out.println(obj1.get_message());
		//Close the contact
		context.close();
		

	}

}
