package practice;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		@SuppressWarnings("unused")
		Triangle triangle=context.getBean("myTriangle",Triangle.class);
		Shape s=(Shape) context.getBean("circle");
		s.draw();
		context.close();

	}

}
