package practice;

public class Circle implements Shape {
	private Point center;
	

	public Point getCenter() {
		return center;
	}


	public void setCenter(Point center) {
		this.center = center;
	}


	@Override
	public void draw() {
		System.out.println("Center of the Circle is :("+center.getX()+","+center.getY());
		System.out.println("Circle drawn");

	}

}
