package com.infotech.people.managementapp;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/*import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import com.infotech.people.managementapp.entities.Person;
//import com.infotech.people.managementapp.entities.Person;
import com.infotech.people.managementapp.service.PeopleManagementService;

@SpringBootApplication
@EnableAsync
public class PeopleManagementSpringBootAppApplication implements CommandLineRunner {
	
	@Autowired
	private PeopleManagementService peopleManagementService;

	public static void main(String[] args) {
		SpringApplication.run(PeopleManagementSpringBootAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		CompletableFuture<Person> completableFuture=peopleManagementService.findByEmail("sandip@gmail.com");
		Person person=completableFuture.get(20, TimeUnit.SECONDS);
		System.out.println(person);
		
	}
		

		
		
	
}
