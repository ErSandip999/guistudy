package com.infotech.people.managementapp.service;


import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infotech.people.managementapp.dao.PeopleManagementDao;
import com.infotech.people.managementapp.entities.Person;

@Service
public class PeopleManagementService {
	@Autowired
	private PeopleManagementDao peopleManagementDao;

	public CompletableFuture<Person> findByEmail(String email) {
		
		return peopleManagementDao.findByEmail(email);
	}



}
