package com.infotech.people.managementapp.dao;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

import com.infotech.people.managementapp.entities.Person;

public interface PeopleManagementDao extends CrudRepository<Person,Integer>{
	List<Person> findIdByLastName(String lastName);
	List<Person>findIsByFirstNameAndEmail(String firstName,String lastName);
	
	@Async
	CompletableFuture<Person> findByEmail(String email);

}
