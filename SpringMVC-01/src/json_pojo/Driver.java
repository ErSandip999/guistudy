package json_pojo;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Driver {

	public static void json_to_pojo1() {
		try {
			// Create object mapper
			ObjectMapper mapper = new ObjectMapper();
			// read JSON file and map/convert to java POJO
			Student student = mapper.readValue(new File("data/student.json"), Student.class);
			// print JSON data
			System.out.println(student);
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}

	public static void json_to_pojo2() {
		try {
			// Create object mapper
			ObjectMapper mapper = new ObjectMapper();
			// read JSON file and map/convert to java POJO
			NewStudent newStudent = mapper.readValue(new File("data/new_student.json"), NewStudent.class);
			// print JSON data
			System.out.println(newStudent);
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	public static void pojo_to_json1() {
		try {
			// Create object mapper
			ObjectMapper mapper = new ObjectMapper();
			
			// read JSON file and map/convert to java POJO
			Student student = mapper.readValue(new File("data/student.json"), Student.class);
			// print JSON data
			System.out.println(student);			
			//POJO to JSON
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			String str_json = mapper.writeValueAsString(student);
		    System.out.println(str_json);
		    
		    //Write Json on File
		    FileOutputStream fileOutputStream = new FileOutputStream("data/_student.json");
		    mapper.writeValue(fileOutputStream, str_json);
		    fileOutputStream.close();
		      
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	public static void main(String[] args) {
		pojo_to_json1();
		//json_to_pojo2();
	}
}