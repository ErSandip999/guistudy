package pkg0;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class InsertRecord {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();		
		Session session = factory.getCurrentSession();		
		try {
			
			//Create new Record
			//Create a student object
			Student student1 = new Student("Raj","Thapa","raj1@gamil.com");
			//start transaction
			session.beginTransaction();
			//save student object
			session.save(student1);
			//commit transaction
			session.getTransaction().commit();									
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}