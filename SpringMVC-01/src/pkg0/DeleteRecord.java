package pkg0;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteRecord {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();		
		Session session = factory.getCurrentSession();		
		try {
			//Delete Record
			int student_id = 1;						
			session.beginTransaction();
			Student student1 = session.get(Student.class, student_id);
			session.delete(student1);		
			session.getTransaction().commit();									
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}