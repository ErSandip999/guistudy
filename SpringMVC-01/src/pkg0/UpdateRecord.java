package pkg0;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UpdateRecord {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();		
		Session session = factory.getCurrentSession();		
		try {
			//Update Record
			int student_id = 1;						
			session.beginTransaction();
			Student student1 = session.get(Student.class, student_id);
			student1.setFirstName("Rajan");
			student1.setLastName("Rimal");			
			session.getTransaction().commit();									
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}