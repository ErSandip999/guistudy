package pkg0;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class QueryingRecords {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();		
		Session session = factory.getCurrentSession();
		
		try {			
			session.beginTransaction();
			
			/*
			//Query All Student
			List<Student> students = session.createQuery("from Student").list();
			for(Student tmp: students) {
				System.out.println(tmp);
			}
			
			
			//Query All Student: last_name="Rai"
			List<Student> students = session
					.createQuery("from Student s where s.lastName='Rai'")
					.list();
			for(Student tmp: students) {
				System.out.println(tmp);
			}
			
			
			List<Student> students = session
					.createQuery("from Student s where s.lastName='Thapa' OR s.firstName='Kiran'")
					.list();
			
			for(Student tmp: students) {
				System.out.println(tmp);
			}
			*/
			
			List<Student> students = session
					.createQuery("from Student s where s.email LIKE '%com%'")
					.list();
			
			for(Student tmp: students) {
				System.out.println(tmp);
			}
			
			
			session.getTransaction().commit();			
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}