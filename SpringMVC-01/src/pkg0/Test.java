package pkg0;

import java.sql.Connection;
import java.sql.DriverManager;

public class Test {
	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/test?useSSL=false";
		String user="root";
		String pass="";
		try {
			System.out.println("Connecting database server");
			Connection conn= DriverManager.getConnection(url, user, pass);
			System.out.println("Connection sucess");
			conn.close();
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}		
	}
}
