package pkg4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("loginObj")
@RequestMapping("/login")
public class LoginController {
	
	@RequestMapping("/showForm")
	public ModelAndView showForm() {
		 return new ModelAndView("login_form");
	}
	
	@RequestMapping("/processForm")
	public ModelAndView processForm(HttpServletRequest request) {
		if(request.getParameter("txt_id").equals("admin") && request.getParameter("txt_pw").equals("admin")) { 
			Login login =  new Login(request.getParameter("txt_id"), request.getParameter("txt_pw"));
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.addObject("loginObj", login);
			modelAndView.setViewName("login_confirmation");
			return modelAndView;
		}
		else {
			return new ModelAndView("login_form");
		}
	}
	@RequestMapping("/displaySession")
	public ModelAndView processForm(HttpSession session) {					
		return new ModelAndView("display_session");
	}	
}
//References
//http://www.kscodes.com/spring-mvc/spring-mvc-session-attributes/