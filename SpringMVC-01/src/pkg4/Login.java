package pkg4;

public class Login {
	private String loginName;
	private String loginPassword;
	
	public Login() {	
		this.loginName = "";
		this.loginPassword = "";
	}
	
	public Login(String loginName, String loginPassword) {	
		this.loginName = loginName;
		this.loginPassword = loginPassword;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	@Override
	public String toString() {
		return "Login [loginName=" + loginName + ", loginPassword=" + loginPassword + "]";
	}
}
