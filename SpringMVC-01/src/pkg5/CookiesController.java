package pkg5;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/cookies")
public class CookiesController {
	
	@RequestMapping("/cookiesHome")
	public ModelAndView readCookieValue() {
		ModelAndView modelAndview= new ModelAndView("cookie_home");
		return (modelAndview);
	}
	
	@RequestMapping("/readCookie")
	public ModelAndView readCookieValue(
			@CookieValue(value = "myCookie", defaultValue = "test123") String myCookie) {
		ModelAndView mv = new ModelAndView("cookie_read");
		mv.addObject("myCookie", myCookie);
		return mv;
	}
 
	@RequestMapping(value = "/writeCookie")
	public String hello(@CookieValue(value = "myCookie", defaultValue = "1") Long hitCounter, HttpServletResponse response) {
		hitCounter++;
		Cookie cookie = new Cookie("myCookie", hitCounter.toString());
		response.addCookie(cookie);
		return "cookie_write";
	}
}