package pkg3;

public class Course {
	
	@CourseCode(value="BIN", message="must start with BIN")
	private String courseCode;

	public Course() {
		this.courseCode = "";
	}

	public Course(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	@Override
	public String toString() {
		return "Course [courseCode=" + courseCode + "]";
	}
}