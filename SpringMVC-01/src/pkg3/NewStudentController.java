package pkg3;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/newStudent")
public class NewStudentController {
	

	//add an initbinder to convert trim input strings
	//remove leading and training whitespce
	//resolve issue for our validation	
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor =  new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
	
	@RequestMapping("/showForm")
	public String showForm(Model newStudentModel) {
		NewStudent newstudent = new NewStudent();
		newStudentModel.addAttribute("newStudent", newstudent);
		return "new_student_form";
	}	
	
	@RequestMapping("/processForm")
	public String processForm(@Valid @ModelAttribute("newStudent") 
			NewStudent newStudent, BindingResult theBindingResult) {		
		if(theBindingResult.hasErrors()) {
			//Redirect to form
			return "new_student_form";
		}
		else {
			System.out.println(newStudent);
			return "new_student_confirmation";
		}		
	}
	
}