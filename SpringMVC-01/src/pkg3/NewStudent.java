package pkg3;

import javax.validation.constraints.Pattern;

public class NewStudent {

	@Pattern(regexp = "^[a-zA-Z0-9]{5}", message = "chars or digirs with max 5 only")
	private String postalCode;

	public NewStudent() {
		this.postalCode = "";
	}

	public NewStudent(String id) {
		this.postalCode = id;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return "NewStudent [postalCode=" + postalCode + "]";
	}
}