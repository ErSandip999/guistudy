package crud_blob;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Main1 {
	
	public static void insert_image(String file_name) {
		try {
			String mysqlUrl = "jdbc:mysql://localhost/test";
			Connection conn = DriverManager.getConnection(mysqlUrl, "root", "");
			PreparedStatement pstmt = conn.prepareStatement("INSERT INTO tbl_images VALUES(?, ?)");
			InputStream in = new FileInputStream(new File(file_name));
			pstmt.setInt(1, 0); //Auto increment and PK
			pstmt.setBlob(2, in); //Photo
			pstmt.execute();
			pstmt.close();
			conn.close();
			System.out.println("Insert record sucessfully");
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
	
	public static void get_image(int id) {
		try {
			String mysqlUrl = "jdbc:mysql://localhost/test";
			Connection conn = DriverManager.getConnection(mysqlUrl, "root", "");
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM tbl_images WHERE id = ?");
			String image_path="C:/Users/info_/Desktop/"+id+".jpg"; 			
			pstmt.setInt(1, 1); //Auto increment and PK			
			ResultSet rs = pstmt.executeQuery();						 
			while(rs.next()) {
				 Blob blob = rs.getBlob("photo");
				 byte byteArray[] = blob.getBytes(1,(int)blob.length());
				 FileOutputStream outPutStream = new FileOutputStream(image_path);
				 outPutStream.write(byteArray);
				 System.out.println("Read image from database and write on file on "+image_path);
			}
			rs.close();
			pstmt.close();
			conn.close();						
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
	
	public static void main(String[] args) {
		try {
			//String image_name="";
			//int id = 1;
			//insert_image(image_name);
			//get_image(id);
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
}