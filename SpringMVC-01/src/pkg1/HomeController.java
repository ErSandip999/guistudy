package pkg1;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	private static final String UPLOAD_DIRECTORY = "uploads";
	private static final int MAX_FILES = 2;
	
	@Autowired
	private Student student;
	 
	@RequestMapping("/one")
	public String one() {
		return "one";
	}

	@RequestMapping("/two")
	public String two(Model model) {
		model.addAttribute("id", this.student.getId());
		model.addAttribute("fullName", this.student.getFullName());
		model.addAttribute("objStudent", this.student);
		System.out.println(student);
		return "two";
	}
	
	@RequestMapping("/_two")
	public String _two(ModelMap map) {
		 map.addAttribute("id", this.student.getId());
		 map.addAttribute("fullName", this.student.getFullName());
		 map.addAttribute("objStudent", this.student);
		 return "two";		    			   
	}
	
	@RequestMapping("/__two")
	public ModelAndView __two() {
		ModelAndView modelAndView = new ModelAndView("two");
	    modelAndView.addObject("id", this.student.getId());
	    modelAndView.addObject("fullName", this.student.getFullName());
	    modelAndView.addObject("objStudent", this.student);	    
	    return modelAndView;	    
	}
	
	@RequestMapping("/redirect")
	public String redirect(Model model) {
		Student student2 = new Student(2,"Krishna");
		model.addAttribute("student2", student2);
		return "redirect:___two";
	}
	
	@RequestMapping("/___two")
	public ModelAndView ___two() {
		ModelAndView modelAndView = new ModelAndView("two_");
		modelAndView.addObject("student2", student);	  
	    return modelAndView;
	}
	
	//Static Contents-1
	@RequestMapping("/three")
	public String three() {
		return "three";
	}
	
	
	//Static Contents-2
	@RequestMapping("/four")
	public String four() {
		return "four";
	}
	
	//Bootstrap Integration-1
	@RequestMapping("/five")
	public String five() {
		return "five";
	}
	
	//Bootstrap Integration-2
	@RequestMapping("/six")
	public String six() {
		return "six";
	}
		
	//Web Form-1
	@RequestMapping("/seven")
	public String seven() {
		return "seven";
	}
	
	//Web Form-1-1
	@RequestMapping("/eight")
	public String eight() {
		return "eight";
	}
	
	//Web Form-2
	@RequestMapping("/nine")
	public String nine() {
		return "nine";
	}
	
	@RequestMapping("/ten")
	public String nine(HttpServletRequest request, Model model) {		
		double n1, n2, n3;
		n1 = Double.parseDouble(request.getParameter("txt_n1"));
		n2 = Double.parseDouble(request.getParameter("txt_n2"));
		n3 = n1+n2;
		model.addAttribute("n1", n1);
		model.addAttribute("n2", n2);
		model.addAttribute("n3", n3);
		return "ten";
	}	
	
	//Web Form-3
	@RequestMapping("/eleven")
	public String eleven() {
		return "eleven";
	}
		
	@RequestMapping("/showForm1")
	public String showForm() {
		// display upload form
		return "upload-form";
	}

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String uploadForm(@RequestParam CommonsMultipartFile file, HttpSession session, Model model)
			throws Exception {
		// Upload file
		try {
			ServletContext context = session.getServletContext();
			String path = context.getRealPath(UPLOAD_DIRECTORY);
			String filename = file.getOriginalFilename();
			System.out.println(path + File.separator + filename);
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(new File(path + File.separator + filename)));
			stream.write(bytes);
			stream.flush();
			stream.close();
			model.addAttribute("message", "upload file sucessfully");
			return "confirmation-message";
		} catch (Exception ex) {
			model.addAttribute("message", "Error: " + ex.getMessage());
			return "upload-form";
		}
	}

	@RequestMapping("/showForm2")
	public String showForm2() {
		// display upload form
		return "upload-multiple-form";
	}

	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files, HttpSession session, Model model) {
		// if (files.length != names.length)
		// return "Mandatory information missing";
		String messages[] = new String[MAX_FILES];
		ServletContext context = session.getServletContext();

		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				String path = context.getRealPath(UPLOAD_DIRECTORY);
				String filename = file.getOriginalFilename();
				System.out.println("File : " + file);
				System.out.println("Name : " + name);
				System.out.println(path + File.separator + filename);
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(path + File.separator + filename)));
				stream.write(bytes);
				stream.flush();
				stream.close();
				messages[i] = "Upload " + filename + " sucessfully\n";
			} catch (Exception e) {
				messages[i] = "Error: " + e.getMessage() + "\n";
			}
		}
		model.addAttribute("messages", messages);
		return "confirmation-message2";
	}

	@RequestMapping("/composeEmail1")
	public String showEmalForm1() {
		// display upload form
		return "compose-email1";
	}

	@RequestMapping("/sendEmail1")
	public String sendEmail1(HttpServletRequest request, Model model) {
		// takes input from e-mail form
		try {
			String fromAddress = "info@testmail.com";
			String toAddress = request.getParameter("recipient");
			String subject = request.getParameter("subject");
			String message = request.getParameter("message");

			// prints debug info
			System.out.println("From : " + fromAddress);
			System.out.println("To: " + toAddress);
			System.out.println("Subject: " + subject);
			System.out.println("Message: " + message);

			// creates a simple e-mail object
			EmailAPI email = new EmailAPI();
			email.sendEmail1(toAddress, subject, message);
			model.addAttribute("message", "Email Send Sucessfully");
		} catch (Exception ex) {
			model.addAttribute("message", "Error : " + ex.getMessage());

		}
		// forwards to the view named "Result"
		return "email-confirmation1";
	}

	@RequestMapping("/composeEmail2")
	public String showEmalForm2() {
		// display upload form
		return "compose-email2";
	}

	@RequestMapping("/sendEmail2")
	public String sendEmail2(HttpServletRequest request, HttpSession session, final @RequestParam CommonsMultipartFile attachFile,
			Model model) {
		try {
			// reads form input
			final String toAddress = request.getParameter("recipient");
			final String subject = request.getParameter("subject");
			final String message = request.getParameter("message");
	
			// for logging
			System.out.println("toAddress: " + toAddress);
			System.out.println("subject: " + subject);
			System.out.println("message: " + message);
			System.out.println("attachFile: " + attachFile.getOriginalFilename());
	
			//Save Uploaded File
			List<File> uploadedFiles=new ArrayList();
			ServletContext context = session.getServletContext();
			String path = context.getRealPath(UPLOAD_DIRECTORY);
			String filename = attachFile.getOriginalFilename();					
			String uploadedFile =path + File.separator + filename; 
			System.out.println(uploadedFile);
			byte[] bytes = attachFile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(path + File.separator + filename)));
			stream.write(bytes);
			stream.flush();
			stream.close();	
			uploadedFiles.add(new File(uploadedFile));
			
			// creates a simple e-mail object
			EmailAPI email = new EmailAPI();
			email.sendEmailWithAttachment1(toAddress, subject, message, uploadedFiles);
			model.addAttribute("message", "Email Send Sucessfully");
		}
		catch (Exception ex) {
			model.addAttribute("message", "Error : " + ex.getMessage());
		}
		return "email-confirmation2";
	}
}