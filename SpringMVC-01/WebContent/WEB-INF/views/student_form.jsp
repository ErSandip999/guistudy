<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "form" uri="http://www.springframework.org/tags/form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Web Form-3 (Spring Form Tags)</title>
</head>
<body>
	<form:form action="processForm" modelAttribute="student">
		<p>First name :	<form:input path="firstName" /></p>
		<p>Last name :	<form:input path="lastName" /></p>
		<p><input type="submit" value="SEND"></p>
	</form:form>
	
</body>
</html>