<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Email Send Confirmation Page</title>
</head>
<body>
	<h2>Email Message</h2>
	<h3>${message}</h3>
	<p><a href="index.jsp">Home</a> | <a href="composeEmail1">Back</a></p>
</body>
</html>