<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "form" uri="http://www.springframework.org/tags/form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Web Form-3 (Customer Form)</title>
<style>
	.error{
		color:red
	}
</style>
</head>
<body>

	<h3><i>Customer Form (Fill out the form. Asterisk (*) means required.</i></h3>
	<form:form action="processForm" modelAttribute="customer">
		<p>
			First name (*):	<form:input path="firstName" />
			<form:errors path="firstName" cssClass="error"/>
		</p>
		<p>
			Last name (*):	<form:input path="lastName" />
			<form:errors path="lastName" cssClass="error"/>
		</p>
		<p><input type="submit" value="SEND"></p>
	</form:form>
	
</body>
</html>