<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
       
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>File Upload Message</title>
</head>
<body>
	<h3>Upload File Messages</h3>
	<ul>
		<c:forEach var="message" items="${messages}">
		    <li><c:out value="${message}"/></li> 
		</c:forEach>
	</ul>	
	<p><a href="index.jsp">HOME</a></p>
</body>
</html>