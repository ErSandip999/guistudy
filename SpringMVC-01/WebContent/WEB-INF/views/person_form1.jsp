<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "form" uri="http://www.springframework.org/tags/form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Web Form-3 (Customer Form)</title>

<style>
	.error{
		color:red
	}
</style>

</head>
<body>

	<h3><i>Person Form (Fill out the form. Asterisk (*) means required.</i></h3>
	<form:form action="processForm" modelAttribute="person">
		<p>
			ID (*):	<form:input path="personId" />
			<form:errors path="personId" cssClass="error"/>
		</p>
		<p>
			Full Name (*):	<form:input path="fullName" />
			<form:errors path="fullName" cssClass="error"/>
		</p>
		<p><input type="submit" value="SEND"></p>
	</form:form>
	
</body>
</html>