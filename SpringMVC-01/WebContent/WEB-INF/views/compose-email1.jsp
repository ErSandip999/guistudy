<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Sending E-mail-1</h1>
	<form method="post" action="sendEmail1">
		<table border="0">
			<tr>
				<td>To:</td>
				<td><input type="text" name="recipient" size="50" /></td>
			</tr>
			<tr>
				<td>Subject:</td>
				<td><input type="text" name="subject" size="50" /></td>
			</tr>
			<tr>
				<td>Message:</td>
				<td><textarea cols="50" rows="10" name="message"></textarea></td>
			</tr>
			<tr>				
				<td colspan="2" align="center"><input type="submit" value="Send E-mail" /></td>
			</tr>
		</table>
	</form>
</body>
</html>