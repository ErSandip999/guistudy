<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "form" uri="http://www.springframework.org/tags/form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Web Form-3 (Regular Expression)</title>

<style>
	.error{
		color:red
	}
</style>

</head>
<body>

	<h3><i>New Student Form (Fill out the form. Asterisk (*) means required.)</i></h3>
	<form:form action="processForm" modelAttribute="newStudent">
		<p>
			ID [09AZaz]:	<form:input path="postalCode" />
			<form:errors path="postalCode" cssClass="error"/>
		</p>		
		<p><input type="submit" value="SEND"></p>
	</form:form>
		
</body>
</html>