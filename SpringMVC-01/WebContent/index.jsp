<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page of mysite.com</title>
</head>
<body>
<h2>Welcome to MySite.com</h2>
<h3>First MVC Program</h3>
<h4><a href="one">Hello world of Spring</a></h4>
<h3>DI and View Render</h3>
<h4><a href="two">Dependency Injection and View Render-1</a></h4>
<h4><a href="_two">Dependency Injection and View Render-2</a></h4>
<h4><a href="__two">Dependency Injection and View Render-3</a></h4>
<h3>Loading Static Contents</h3>
<h4><a href="three">Static Contents-1</a></h4>
<h4><a href="four">Static Contents-2</a></h4>
<h3>Bootstrap Integration</h3>
<h4><a href="five">Bootstrap Integration-1</a></h4>
<h4><a href="six">Bootstrap Integration-2</a></h4>
<h3>Web Forms</h3>
<h4><a href="seven">Web Form-1</a></h4>
<h4><a href="nine">Web Form-2</a></h4>
<h4><a href="student/showForm">Web Form-3</a></h4>
<h3>Validations</h3>
<h4><a href="customer/showForm">Form Validation - Null </a></h4>
<h4><a href="person/showForm">Form Validation Number </a></h4>
<h4><a href="newStudent/showForm">Regular Expression</a></h4>
<h4><a href="course/showForm">Custom Validation</a></h4>
<h3>File Upload</h3>
<h4><a href="showForm1">Upload File</a></h4>
<h4><a href="showForm2">Upload Multiple Files </a></h4>
<h3>Sending Email</h3>
<h4><a href="composeEmail1">Send Email</a></h4>
<h4><a href="composeEmail2">Send Email with Attachment</a></h4>
<h3>Session</h3>
<h4><a href="login/showForm">Login Form</a></h4>
<h3>Cookies</h3>
<h4><a href="cookies/cookiesHome">Cookies Write and Read</a></h4>
<h3>Page Redirect</h3>
<h4><a href="redirect">Redirect page</a></h4>
</body>
</html>