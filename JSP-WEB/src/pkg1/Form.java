package pkg1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Form extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter();
		response.setContentType("text/html");
		response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String[] languages = request.getParameterValues("language");

		HttpSession session = request.getSession();
		session.setAttribute("id", id);
		session.setAttribute("name", name);
		session.setAttribute("password", password);
		session.setAttribute("languages", languages);

		List<String> language = new ArrayList<>();

		for (int i = 0; i <= languages.length - 1; i++) {
			language.add(languages[i]);
		}
		FormClass fc;
		fc = new FormClass(id, name, password, language);
		session.setAttribute("formclass", fc);

		// out.println(id + " " + name + " " + " " + password);
		response.sendRedirect("Output.jsp");

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
