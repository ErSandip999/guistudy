package pkg1;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

	public static void main(String[] args) {
		MainClass mc = new MainClass();
		List<String> languages = new ArrayList<>();
		languages.add("English");
		languages.add("Nepali");
		mc.setLanguages(languages);

		for (String lang : languages) {
			System.out.println(lang);
		}

	}

}
