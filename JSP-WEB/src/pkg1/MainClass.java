package pkg1;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
	private List<String> languages;

	public MainClass() {
		this.languages = new ArrayList<String>();
	}

	public MainClass(List<String> languages) {
		this.languages = languages;

	}

	public MainClass(MainClass mc) {
		this.languages = mc.languages;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

}
