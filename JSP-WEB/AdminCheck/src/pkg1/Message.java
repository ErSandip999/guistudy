package pkg1;

public class Message {
	User user;
	boolean flag;
	String msg;
	
	
	public Message() {
		this.user=new User();
		this.flag=false;
		this.msg="";
		
	}
	
	
	public Message(User user, boolean flag,String msg) {
		this.user=user;
		this.flag=false;
		this.msg=msg;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public String getMsg() {
		return msg;
	}


	public void setMessage(String msg) {
		this.msg = msg;
	}
	
	
	
	

}
