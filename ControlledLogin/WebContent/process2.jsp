<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<jsp:useBean id="person" class="pkg11.Person" scope="session"></jsp:useBean>
<jsp:setProperty property="*" name="person"/>
<title>Setter and Getter...</title>
</head>
<body>
<%String name= request.getParameter("name");%>
Setting.....<br>
<jsp:setProperty name="person" property="fullName"  value="<%=name %>"/>

Getting.....<br>
<jsp:getProperty name="person" property="fullName" />

<p><a href="Display.jsp">Display</a></p>

</body>
</html>