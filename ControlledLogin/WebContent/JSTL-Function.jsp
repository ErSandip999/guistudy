<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSTL-Function</title>
</head>
<body>
<c:set var="str1" scope="session" value=" It is used for replacing any upper case character "/>
<c:out value="${fn:toUpperCase(str1)}"></c:out><br>


<c:set var="string" value="This is the first string."/>  
${fn:substring(string, 5, 17)}
<br>
<c:set var="str1" value="This is first string"/>  
<c:set var="str2" value="Hello"/>  
Length of the String-1 is: ${fn:length(str1)}<br>  
Length of the String-2 is: ${fn:length(str2)}  
</body>
</html>