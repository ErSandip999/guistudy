package LectureCRUD;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/sortanddisplay")
public class SortAndDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306";
		String dbname = "test";
		String username = "root";
		String password = "";
		Connection conn;
		boolean res = false;

		try {

			String sql = "Select * from  tbl_person ";

			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + "/" + dbname, username, password);
			PreparedStatement pstat = conn.prepareStatement(sql);
			ResultSet rs = pstat.executeQuery();
			List<Staff> stafflist = Collections.synchronizedList(new ArrayList<Staff>());
			while (rs.next()) {
				res = true;
				Staff staff = new Staff(rs.getInt(1), rs.getNString(2), rs.getString(3));
				stafflist.add(staff);

			}

			Collections.sort(stafflist, new Comparator() {
				public int compare(Object stafflist1, Object stafflist2) {
					return ((Staff) stafflist1).full_name.compareToIgnoreCase(((Staff) stafflist2).full_name);
				}
			});
			out.println("<table border=1>");
			out.println("<tr><td>ID</td><td>Name</td><td>Address</td><td>Edit</td><td>Delete</td>");
			for (Staff list : stafflist) {
				response.getWriter().println("<tr><td>" + list.id + "</td><td>" + list.full_name + "</td><td>"
						+ list.contact_address + "</td><td>Edit</td><td>Delete</td></tr>");
			}

			pstat.close();
			conn.close();
			// RequestDispatcher rd;

			if (res == true) {
				response.getWriter().println("<p>Record Founded  and displayed Successfully");

			} else {
				response.getWriter().println("record Not Found");

			}

		} catch (Exception ex) {
			response.getWriter().println("Error:" + ex.getMessage());
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
