package LectureCRUD;

public class Staff {
	int id;
	String full_name;
	String contact_address;

	public Staff() {
		this.id = 0;
		this.full_name = "";
		this.contact_address = "";

	}

	public Staff(int id, String full_name, String contact_address) {
		this.id = id;
		this.full_name = full_name;
		this.contact_address = contact_address;
	}

	public Staff(Staff s) {
		this.id = s.id;
		this.full_name = s.full_name;
		this.contact_address = s.contact_address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getContact_address() {
		return contact_address;
	}

	public void setContact_address(String contact_address) {
		this.contact_address = contact_address;
	}

	@Override
	public String toString() {
		return "Staff [id=" + id + ", full_name=" + full_name + ", contact_address=" + contact_address + "]";
	}

}
