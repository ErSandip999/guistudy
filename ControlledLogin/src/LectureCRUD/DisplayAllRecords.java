package LectureCRUD;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayAllRecords extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306";
		String dbname = "test";
		String username = "root";
		String password = "";
		Connection conn;
		boolean res = false;
		// Connect Database Server
		// Library(*.jar)
		// LoadDrievr
		// Connect(database Server,Username,password)
		// Close Connection
		// CRUD

		try {
			// int id = Integer.parseInt(request.getParameter("txt_id"));
			String sql = "Select * from  tbl_person ";

			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + "/" + dbname, username, password);
			PreparedStatement pstat = conn.prepareStatement(sql);
			// pstat.setInt(1, id);

			ResultSet rs = pstat.executeQuery();

			out.println("<table border=1>");
			out.println("<tr><td>ID</td><td>Name</td><td>Address</td><td>Edit</td><td>Delete</td>");
			while (rs.next()) {
				res = true;
				// response.getWriter().println("ID:" + rs.getInt(1) + "<br>");
				// response.getWriter().println("Name:" + rs.getString(2) + "<br>");
				// response.getWriter().println("Address:" + rs.getString(3) + "<br>");

				out.println("<tr><td>" + rs.getInt(1) + "</td><td>" + rs.getString(2) + "</td><td>" + rs.getString(3)
						+ "</td><td>Edit</td><td>Delete</td></tr>");

				// HttpSession session = request.getSession();
				// session.setAttribute("id", rs.getInt(1));
				// session.setAttribute("name", rs.getString(2));
				// session.setAttribute("address", rs.getString(3));
				// request.setAttribute("id", rs.getInt(1));
				// request.setAttribute("name", rs.getString(2));
				// request.setAttribute("address", rs.getString(3));

			}

			out.println("</table>");

			// response.sendRedirect("form11_2.jsp");
			pstat.close();
			conn.close();
			RequestDispatcher rd;

			if (res == true) {
				response.getWriter().println("<p>Record Founded  and displayed Successfully");
				// rd = request.getRequestDispatcher("form11_3.jsp");
				// rd.include(request, response);
			} else {
				response.getWriter().println("record Not Found");
				// rd = request.getRequestDispatcher("SearchForm2.jsp");
			}

		} catch (Exception ex) {
			response.getWriter().println("Error:" + ex.getMessage());
			// PrintWriter out=response.getWriter();
			// out.println("Error:"+ex.getMessage());

		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
