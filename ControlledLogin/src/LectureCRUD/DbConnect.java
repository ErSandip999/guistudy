package LectureCRUD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DbConnect extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter();
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306";
		String dbname = "test";
		String username = "root";
		String password = "";
		Connection conn;
		// Connect Database Server
		// Library(*.jar)
		// LoadDrievr
		// Connect(database Server,username,passwprd)
		// Close Connection
		// CRUD
		try {

			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + "/" + dbname, username, password);
			conn.close();
			response.getWriter().println("<p>Connect database successfully");

		} catch (Exception ex) {
			response.getWriter().println("Error:" + ex.getMessage());

		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
