package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.Database;


public class NewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	PrintWriter out=response.getWriter();
	response.setContentType("text/html");
	String full_name=request.getParameter("txt_full_name");
	String login_name=request.getParameter("txt_login_name");
	String login_password=request.getParameter("txt_login_pass");
	
	User user=new User(0,full_name,login_name,login_password);
	
	Database db=new Database();
	Message message=db.saveUser(user);
	//System.out.println(message.isFlag());
	
	if(message.flag) {
		out.println("Saved Successfully");
	}
	else {
		out.println("Error: User not saved");
	}
		
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
