package pkg1;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SaveUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String fn= request.getParameter("txt_fn");
		String ln=request.getParameter("txt_ln");
		String lp=request.getParameter("txt_lp");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
			//String sql="insert into tbl_user(full_name,login_name,login_password)values('Krishna','Krishna','Krishna')";
			String sql="insert into tbl_user(full_name,login_name,login_password)values(?,?,?)";
			
			
			PreparedStatement pstat= conn.prepareStatement(sql);
			pstat.setString(1, fn);
			pstat.setString(2,ln);
			pstat.setString(3,lp);
			pstat.executeUpdate();
			
			pstat.close();
			conn.close();
			
			
			
			
			
			
		}catch(Exception ex) {
			out.println("Error:"+ex.getMessage());
		}
		
		
	}
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
