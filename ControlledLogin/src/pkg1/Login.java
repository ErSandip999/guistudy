package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.Database;


public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String login_name=request.getParameter("txt_name");
		String login_password=request.getParameter("txt_password");
		RequestDispatcher rd;
		
		User user=new User();
		user.setLogin_name(login_name);
		user.setLogin_password(login_password);
		Database db=new Database();
		Message message=db.doLogin(user);
		user=message.user;
		//System.out.println(message.user.getFull_name());
		 if(message.flag=true) {
				request.setAttribute("full_name",message.user.getFull_name());
				
				rd=request.getRequestDispatcher("home");
				rd.forward(request,response);
				
				
			}
			else {
				out.println("<h2>Error:Login name or passsword</h2>");
				out.println("<br>Enter username and password again.");
				rd=request.getRequestDispatcher("Login.jsp");
				rd.include(request,response);
				
			}
		
		
	}
       
    
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
