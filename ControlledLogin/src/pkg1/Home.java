package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("Welcome:"+request.getAttribute("full_name")+"<br><br><br>"+"<a href='Login.jsp'>Logout</a>"+"<br>");
		out.println("<h3>Home Page</h3>");
		out.println("<p><a href='NewUser.jsp'>1.ADD USER</a></p>");
		out.println("<p><a href='SaveUser.jsp'>2.SAVE USER</a></p>");
		
		
		out.close();
	}
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
