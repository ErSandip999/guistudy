package pkg11;

public class Person {
	String FullName;

	public Person() {
		this.FullName = "";
	}

	public Person(String fullName) {

		this.FullName = fullName;
	}

	public Person(Person p) {
		this.FullName = p.FullName;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	@Override
	public String toString() {
		return "Person [Fullname=" + FullName + "]";
	}

}
