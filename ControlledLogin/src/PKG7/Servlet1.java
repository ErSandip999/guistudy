package PKG7;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		int num1=Integer.parseInt(request.getParameter("num"));
		
		
		session.setAttribute("v1",num1);
		//or set the value n1=20; and then use the code as session .setattrinute("v1","n1");
		
		out.println("V1="+session.getAttribute("v1"));
		out.println("<br>Hello From servlet1<br>");
		out.println("<a href='servlet22'>Servlet2</a>");
		out.println("<a href='servlet33'>Servlet3</a>");
		
		
	}

       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
		
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
