package NewServlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out =response.getWriter();
		
		ServletContext sc=getServletContext();
		String site_name=sc.getInitParameter("site_name");
		
		ServletConfig sf=getServletConfig();
		String servlet_name=sf.getInitParameter("servlet_title");
		
		 out.println("Site Name:"+site_name);
		 out.println("<br>Servlet Name:"+servlet_name);
		 
		 //HttpSession session =request.getSession();
		 //session.setAttributes("id","1");
		 //String str_id=(String)session.getAttribute("id");
		 //out.println("ID:"+str_id);
		
		 
		out.println(site_name);
		out.println("<br>Hello From servlet1<br>");
		out.println("<a href='servlet1'>Servlet2</a>");
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doProcess(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
