package pkgCRUD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Insert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String full_name = request.getParameter("full_name");
		String contact_address = request.getParameter("contact_address");

		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306/";
		String db_name = "test";
		String username = "root";
		String userPassword = "";
		Connection conn;
		response.sendRedirect("Decision.jsp");

		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + db_name, username, userPassword);
			String sql = "insert into tbl_person(full_name,contact_address) values(?,?)";
			PreparedStatement pstat = conn.prepareStatement(sql);
			pstat.setString(1, full_name);
			pstat.setString(2, contact_address);
			pstat.executeUpdate();
			pstat.close();
			conn.close();
		} catch (Exception e) {
			response.getWriter().print(e.getMessage());
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
