package PKG10;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class filter3 implements Filter {

	@Override
	public void destroy() {
		System.out.println("destroy method of filter 3");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// check range
		int num1 = Integer.parseInt(request.getParameter("txt_num1"));
		if (num1 >= 1 && num1 <= 100) {
			chain.doFilter(request, response);
		} else {
			System.out.println("Number is out of range");
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.println("Error: Number out of range");
			RequestDispatcher rd = request.getRequestDispatcher("form10.jsp");
			rd.include(request, response);
		}

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("hello  from init of filtre 3");
	}

}
