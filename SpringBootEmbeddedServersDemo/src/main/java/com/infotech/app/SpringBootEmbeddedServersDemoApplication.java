package com.infotech.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@ComponentScan(basePackages="")
public class SpringBootEmbeddedServersDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEmbeddedServersDemoApplication.class, args);
	}

}

@RestController
 class MyController implements ErrorController{
	private static final String PATH="/error";
	@GetMapping("/")
	public String welcome() {
		return "Hello";
	}
	
	@GetMapping(PATH)
	public String defaultErrorMessage() {
		return "Request URL Doesnot EXist....";
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return PATH;
	}
	
}
