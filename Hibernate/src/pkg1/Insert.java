package pkg1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Insert {

	public static void main(String[] args) {
		SessionFactory factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		Session session=factory.getCurrentSession();
		try {
			Student s1=new Student(6,"Raj","Kandel","raj@gmail.com");
			
			//Start translation
			session.beginTransaction();
			
			//save student object
			session.save(s1);
			
			//commit translation
			session.getTransaction().commit();
			
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
		}

	}

}
