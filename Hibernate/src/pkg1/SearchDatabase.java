package pkg1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SearchDatabase {

	public static void main(String[] args) {
		SessionFactory factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		Session session=factory.getCurrentSession();
		try {
		session.beginTransaction();
		Student s1=session.get(Student.class, 1);
		session.getTransaction().commit();
		session.close();
		System.out.println("Student Details:"+s1);
		}catch(Exception ex) {
			
		}

	}

}
