package pkg1;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Querry2 {

	public static void main(String[] args) {
		SessionFactory factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		Session session=factory.getCurrentSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Student>students=session.createQuery("from Student s where s.lastName='Agrahari'").list();
		for(Student temp:students) {
			System.out.println(temp);
		}
		

	}

}
