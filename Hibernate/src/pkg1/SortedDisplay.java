package pkg1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SortedDisplay {

	@SuppressWarnings({ "unchecked" })
	public static void main(String[] args) {
		SessionFactory factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		Session session=factory.getCurrentSession();
		List<Student> students = new ArrayList<Student>();
		session.beginTransaction();
		
	    students=session.createQuery("from Student").list();
		Collections.sort(students, new Comparator<Object>() {
			public int compare(Object s1, Object s2) {
				return ((Student) s1).getFirstName().compareToIgnoreCase(((Student) s2).getFirstName());
			}
		});
		for(Student temp:students) {
			System.out.println(temp);
		}
		
		

	}

}
