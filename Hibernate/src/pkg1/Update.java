package pkg1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Update {

	public static void main(String[] args) {
		SessionFactory factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		Session session=factory.getCurrentSession();
		int student_id=1;
		try {
			
		session.beginTransaction();
		Student student1=session.get(Student.class, student_id);
		if(student1!=null) {
		student1.setFirstName("Sandeep");
		student1.setLastName("Agrahari");
		session.getTransaction().commit();
		}
		session.close();
		
		}catch(Exception ex) {
			
		}

	}

}
