package pkg1;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Database {
	SessionFactory factory;
	Session session;
	
	public Database() {
		this.factory=new Configuration().
				configure("hibernate.cfg.xml").
				addAnnotatedClass(Student.class).
				buildSessionFactory();
		this.session=factory.getCurrentSession();
	}
	
	public boolean insertRecord(Student student) {
		boolean result=false;
		try {
			session.beginTransaction();
			session.save(student);
			session.getTransaction().commit();
			result=true;
			
		}catch(Exception ex) {
			result=false;
			
			
		}
		return result;
		
	}
	
	public boolean updateRecord(Student student) {
		boolean result= false;
		try {
			session.beginTransaction();
			Student student1=session.get(Student.class, student.getId());
			if(student1!=null) {
			student1.setFirstName(student.getFirstName());
			student1.setLastName(student.getLastName());
			
			session.getTransaction().commit();
			}
			result=true;
		}catch(Exception ex) {
			result=false;
			
		}
		return result;
	}
	public boolean deleteRecord(Student student) {
		boolean result= false;
		try {
			session.beginTransaction();
			Student student1=session.get(Student.class, student.getId());
			session.delete(student1);
			session.getTransaction().commit();
			result=true;
		}catch(Exception ex) {
			result=false;
			
		}
		return result;
	}
	
	public Student searchRecord(Student student) {
		Student s1=null;
		try {
			session.beginTransaction();
			s1=session.get(Student.class,student.getId());
			session.getTransaction().commit();
			
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
			
		}
		return s1;
	}
	
	@SuppressWarnings("unchecked")
	public List<Student> getAll() {
		List<Student> students=new ArrayList<Student>();
		try {
			session.beginTransaction();
			students=session.createQuery("from Student").list();
			
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
		}
		return students;
	}

		

}
