package pdfHandler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CreatePdf {
	public static void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/pdfFile.pdf"));
			document.open();
			Paragraph para = new Paragraph("This is the first pdf file");
			document.add(para);

			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(105);
			table.setSpacingBefore(11f);
			table.setSpacingAfter(11f);

			float[] colWidth = { 2f, 2f, 2f };
			table.setWidths(colWidth);
			PdfPCell c1 = new PdfPCell(new Paragraph("Column1"));
			PdfPCell c2 = new PdfPCell(new Paragraph("Column2"));
			PdfPCell c3 = new PdfPCell(new Paragraph("Column3"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			document.add(table);

			List orderedlist = new List(List.ORDERED);
			orderedlist.add(new ListItem("Fun"));
			orderedlist.add(new ListItem("Coding"));
			orderedlist.add(new ListItem("Java"));
			document.add(orderedlist);

			List unorderedlist = new List(List.UNORDERED);
			unorderedlist.add(new ListItem("Fun"));
			unorderedlist.add(new ListItem("Coding"));
			unorderedlist.add(new ListItem("Java"));
			document.add(unorderedlist);

			document.close();
			writer.close();
			System.out.println("PDF has been generated...");
		} catch (FileNotFoundException | DocumentException e) {

			e.printStackTrace();
		}

	}

}
