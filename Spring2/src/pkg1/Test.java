package pkg1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		Student student=context.getBean("primary",PrimaryStudent.class);
		student.getInfo();
		//student=context.getBean("secondary",SecondaryStudent.class);
		student=(SecondaryStudent)context.getBean("secondary");
		student.getInfo();
		context.close();
		

	}

}
