package pkg1;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

@SuppressWarnings("deprecation")
public class newTest {

	public static void main(String[] args) {
		
		XmlBeanFactory factory=new XmlBeanFactory(new ClassPathResource("AppContext.xml"));
		Student student=(SecondaryStudent)factory.getBean("secondary");
		student.getInfo();
		student=(PrimaryStudent)factory.getBean("primary");
		student.getInfo();

	}

}
