package pkg1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {
	public static void allCountries() {
		try {
			URL url = new URL("http://localhost:8080/SpringRest/countries/");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();			
			conn.setRequestMethod("GET");
			int responseCode = conn.getResponseCode();
			String readLine = null;			
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in .readLine()) != null) {
		            response.append(readLine);
		        } 
				in .close();
				System.out.println(response.toString());
			}						
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
	
	public static void postCountry() {
		try {
			final String POST_PARAMS = "{\n" + "\"id\": 5,\r\n" +
			        "    \"countryName\": \"Pakistan\",\r\n" +
			        "    \"population\": \"123456789\"" + "\n}";
			System.out.println(POST_PARAMS);
			URL url = new URL("http://localhost:8080/SpringRest/countries/");			
			HttpURLConnection postConnection  = (HttpURLConnection) url.openConnection();			
		    postConnection.setRequestMethod("POST");
		    //postConnection.setRequestProperty("userId", "a1bcdefgh");
		    postConnection.setRequestProperty("Content-Type", "application/json");
		    postConnection.setDoOutput(true);
		    OutputStream os = postConnection.getOutputStream();
		    os.write(POST_PARAMS.getBytes());
		    os.flush();
		    os.close();
		    int responseCode = postConnection.getResponseCode();
		    if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
		    	BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
		    	String inputLine;
		        StringBuffer response = new StringBuffer();		        
		        while ((inputLine = in .readLine()) != null) {
		            response.append(inputLine);
		        } 
		        in .close();		        
				System.out.println(response.toString());
			}	
		}
		catch(Exception ex) {
			System.out.println("Error : "+ex.getMessage());
		}
	}
	
	public static void main(String []args) {		
		allCountries();
		postCountry();
		allCountries();
		
	}
}