<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring RESTfull</title>
</head>
<body>
	<h1>Rest API</h1>
	<h3>GET http://localhost:8080/SpringRest/countries/</h3>
	<h3>GET http://localhost:8080/SpringRest/countries/1</h3>
	<h3>POST http://localhost:8080/SpringRest/countries/</h3>
	<h3>PUT http://localhost:8080/SpringRest/countries/1</h3>
	<h3>DELETE http://localhost:8080/SpringRest/countries/1</h3>
</body>
</html>