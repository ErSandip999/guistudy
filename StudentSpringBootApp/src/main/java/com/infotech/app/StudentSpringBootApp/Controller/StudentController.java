package com.infotech.app.StudentSpringBootApp.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
	
	//@RequestMapping(value="/", method=RequestMethod.GET) is same as
	//@GetMapping("/") or
	@GetMapping(value="/")
	public String sayHello() {
		return "Hello SpringBoot Learner";
	}

}
