package Client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.Message;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("AppContext.xml");
		Message m1=context.getBean("message",Message.class);
		m1.setId(1);
		m1.setMessage("Hello from m1");
		System.out.println(m1);
		
		Message m2=context.getBean("message",Message.class);
		m2.setId(2);
		m2.setMessage("Hello from m2");
		System.out.println(m2);
		((AbstractApplicationContext)context).close();
	}

}
