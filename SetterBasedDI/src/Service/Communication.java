package Service;

import Message.Messaging;

public class Communication {
	private Messaging message;

	public Messaging getMessage() {
		return message;
	}

	public void setMessage(Messaging message) {
		this.message = message;
	}
	public void communicate() {
		message.sendMessage();
	}
	

}
