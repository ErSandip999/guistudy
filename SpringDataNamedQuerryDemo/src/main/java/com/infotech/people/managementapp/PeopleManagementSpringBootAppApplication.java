package com.infotech.people.managementapp;

import java.util.List;

/*import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.infotech.people.managementapp.entities.Person;
//import com.infotech.people.managementapp.entities.Person;
import com.infotech.people.managementapp.service.PeopleManagementService;

@SpringBootApplication
public class PeopleManagementSpringBootAppApplication implements CommandLineRunner {
	
	@Autowired
	private PeopleManagementService peopleManagementService;

	public static void main(String[] args) {
		SpringApplication.run(PeopleManagementSpringBootAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//createPerson();
		//createPersons();
		//getPersonByIds();
		//getPersonsInfoByLastName();
		getPersonsInfoByFirstNameAndEmail();
		
		
		
	}

	private void getPersonsInfoByFirstNameAndEmail() {
		List<Person> personList=peopleManagementService.getPersonsInfoByFirstNameAndEmail("Sandip","sandip@gmail.com");
		for(Person temp:personList) {
			System.out.println(temp);
		}
	}

	private void getPersonsInfoByLastName() {
		List<Person> personList=peopleManagementService.getPersonsInfoByLastName("Agrahari");
		for(Person temp:personList) {
			System.out.println(temp);
		}
		
	}

	/*
	 * private void getPersonByIds() { List<Integer> ids=new ArrayList<Integer>();
	 * ids.add(2); ids.add(3);
	 * Iterable<Person>personList=peopleManagementService.getPersonsbyIds(ids);
	 * for(Person temp:personList) { System.out.println(temp); }
	 * 
	 * }
	 * 
	 * private void createPersons() { List<Person> personList=Arrays.asList(new
	 * Person("Barry","Johnson","barry33@gmail.com",new Date()), new
	 * Person("Sandeep","Agrahari","sandip999@gmail.com",new Date()));
	 * peopleManagementService.createPersons(personList);
	 * 
	 * }
	 * 
	 * private void createPerson() { Person p1=new Person("Sandip",
	 * "Agrahari","sandip@gmail.com",new Date());
	 * peopleManagementService.createPerson(p1);
	 * 
	 * }
	 */
}
