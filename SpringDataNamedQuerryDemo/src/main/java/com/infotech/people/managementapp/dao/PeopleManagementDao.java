package com.infotech.people.managementapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.infotech.people.managementapp.entities.Person;

public interface PeopleManagementDao extends CrudRepository<Person,Integer>{
	
	@Query(value ="Select p FROM Person p WHERE p.lastName=?1")
	List<Person> findIdByLastName(String lastName);
	
	@Query(value="Select p FROM Person p WHERE p.firstName=?1 AND p.email=?2")
	//"for native named native query we have to add an attributre nativeQuery=true;"
	List<Person>findIsByFirstNameAndEmail(String firstName,String lastName);

}
